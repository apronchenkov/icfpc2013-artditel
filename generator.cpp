#include "generator.h"

#include <set>


struct T {
    T(size_t b):a(0),b(b), c(b/100 + 1) { std::cerr << "starting: " << c << " " << b << std::endl; }

    void inc() {
        ++a;
        if (a % c == 0)
            std::cerr << "step: " << a / c << " now: " << a << " total: " << b << std::endl;
    }
    size_t a,b,c;
};

void generate(int n, HashTable& table) {

    static const std::vector<Op> unary = {Op::Shl1, Op::Not, Op::Shr1, Op::Shr4, Op::Shr16};
    static const std::set<Op> shifts = {Op::Not, Op::Shl1, Op::Shr1, Op::Shr4, Op::Shr16};
    static const std::vector<Op> binary = {Op::And, Op::Or, Op::Xor, Op::Plus};
    static const std::vector<Op> ternary = {Op::If0};
    std::vector<Bits> output_bits;
    size_t x =  unary.size() * table.getVector(n - 1).size();
    for (int length = 1; length <= (n - 1) / 2; length++) {
        const auto& leftExprs = table.getVector(length);
        const auto& rightExprs = table.getVector(n - length - 1);
        x += binary.size() * leftExprs.size() + rightExprs.size();
    }
    for (int first_length = 1; first_length <= n - 3; first_length++) {
        for (int second_length = 1; second_length <= n - first_length - 2; second_length++) {
            x += table.getVector(first_length).size() * table.getVector(second_length).size() * table.getVector(n - first_length - second_length - 1).size();
    }}

    T t(x);

    for (int i = 0; i < unary.size(); i++) {
        auto op = unary[i];
        for (CExpression  expression: table.getVector(n - 1)) {
            t.inc();
            if (expression->isZero() && op != Op::Not) {
                continue;
            }
            bool found = 0;
            if (i >= 2) {
                for (int j = i + 1; j < unary.size(); j++) {
                    if (unary[j] == expression->op()) {
                        found = 1;
                    }
                }
            }
            if (found || (op == Op::Not && expression->op() == Op::Not)) {
                continue;
            }

            auto tribool = Expression(op, expression).tribool();
            CExpression treePtr = expression;
            found = false;
            while (shifts.count(treePtr->op())) {
                if (treePtr->tribool() == tribool) {
                    found = true;
                    break;
                }
                treePtr = &treePtr->arg(0);
            }
            if (found) {
                continue;
            }

            table.insert(n, Expression::allocate(Expression(op, expression)));
        }
    }

    for (auto op : binary) {
        for (int length = 1; length <= (n - 1) / 2; length++) {
            const auto& leftExprs = table.getVector(length);
            const auto& rightExprs = table.getVector(n - length - 1);
            for (size_t i = 0; i < leftExprs.size(); ++i) {
                CExpression left_expression = leftExprs[i];
                if (left_expression->isZero()) {
                    continue;
                }
                for (size_t j = (length == n - length - 1 ? i + 1 : 0); j < rightExprs.size(); ++j) {
                    CExpression right_expression = rightExprs[j];

                    t.inc();
                    if (right_expression->isZero()) {
                        continue;
                    }

                    if (right_expression->op() == Op::Not && (
                                op != Op::Plus && left_expression->op() == Op::Not || // not a op not b -> not (a op' b)
                                left_expression == &right_expression->arg(0))) { // a op not a -> const
                        continue;
                    }
                    if (op != Op::Plus) {
                        auto t1 = left_expression->tribool();
                        auto t2 = right_expression->tribool();
                        if (!~(t1.set | t2.set)) {

                            if (eval(t1.determine(1), t2, op) == t1.determine(1) &&
                                    eval(t1.determine(0), t2, op) == t1.determine(0) ||
                                    eval(t2.determine(1), t1, op) == t2.determine(1) &&
                                    eval(t2.determine(0), t1, op) == t2.determine(0)) {
                                continue;
                            }
                        }
                    }

                    table.insert(n, Expression::allocate(Expression(op, left_expression, right_expression)));
                }
            }
        }
    }

    for (auto op : ternary) {
        for (int first_length = 1; first_length <= n - 3; first_length++) {
            for (CExpression first_expression: table.getVector(first_length)) {
                if (first_expression->op() == Op::One ||
                        first_expression->op() == Op::Not ||
                        first_expression->isConst()) {
                    continue;
                }
            for (int second_length = 1; second_length <= n - first_length - 2; second_length++) {
                    for (CExpression second_expression : table.getVector(second_length)) {
                        for (CExpression third_expression : table.getVector(n - first_length - second_length - 1)) {
                            t.inc();
                            if (second_expression == third_expression) continue;
                            table.insert(n, Expression::allocate(Expression(
                                            op,
                                            first_expression,
                                            second_expression,
                                            third_expression)));
                        }
                    }
                }
            }
        }
    }

}

void generate0(int n, HashTable& table) {

    static const std::vector<Op> unary = {Op::Shl1, Op::Not, Op::Shr1, Op::Shr4, Op::Shr16};
    static const std::set<Op> shifts = {Op::Not, Op::Shl1, Op::Shr1, Op::Shr4, Op::Shr16};
    static const std::vector<Op> binary = {Op::And, Op::Or, Op::Xor, Op::Plus};
    static const std::vector<Op> ternary = {Op::If0};
    std::vector<Bits> output_bits;
    size_t x =  0;
    for (int first_length = n/3 - 2; first_length <= n/3+2; first_length++) {
        for (CExpression first_expression: table.getVector(first_length)) {
            if (first_expression->op() == Op::Not ||
                    first_expression->isConst()) {
                continue;
            }
            for (int second_length = n/3-2; second_length <= n/3+2; second_length++) {
                    if (n - first_length - second_length - 1 < n/3-2 || n - first_length - second_length - 1 > n/3 + 2)
                        continue;
                x += table.getVector(n - first_length - second_length - 1).size() * table.getVector(second_length).size() * table.getVector(first_length).size();
            }
        }
    }

    T t(x);


    Op op = Op::If0;
    for (int first_length = n/3 - 2; first_length <= n/3+2; first_length++) {
        for (CExpression first_expression: table.getVector(first_length)) {
            if (first_expression->op() == Op::Not ||
                    first_expression->isConst()) {
                continue;
            }
        for (int second_length = n/3-2; second_length <= n/3+2; second_length++) {
                for (CExpression second_expression : table.getVector(second_length)) {
                    if (n - first_length - second_length - 1 < n/3-2 || n - first_length - second_length - 1 > n/3 + 2)
                        continue;
                    for (CExpression third_expression : table.getVector(n - first_length - second_length - 1)) {
                        t.inc();
                        if (second_expression == third_expression) continue;
                        table.insert(n, Expression::allocate(Expression(
                                        op,
                                        first_expression,
                                        second_expression,
                                        third_expression)));
                    }
                }
            }
        }
    }

}

