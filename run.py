#!/usr/bin/env python

from argparse import ArgumentParser
import urllib2
import json
import time
import subprocess
from datetime import datetime, timedelta

URL_PATTERN = "http://icfpc2013.cloudapp.net/{0}?auth=0260HTHBSxr3vEPhJalmVnMakhAxdAduQfchb9mvvpsH1H"
TIME_WINDOW = timedelta(seconds=20)
MAX_REQUESTS = 5


def request_(handle, data = None, request_history = []):
    print "requesting", handle
    if len(request_history) >= 5:
        delta = (datetime.now() - request_history[-MAX_REQUESTS])
        if delta < TIME_WINDOW:
            delta = TIME_WINDOW - delta
            wait = delta.seconds + delta.microseconds * 1e-6
            print "Waiting {0} seconds".format(wait)
            time.sleep(wait)

    request_history.append(datetime.now())

    req = urllib2.Request(URL_PATTERN.format(handle))
    response = urllib2.urlopen(req, data=(json.dumps(data) if data else ""))
    print "Request: {0}, status: {1}".format(handle, response.getcode())
    return json.load(response) if response.getcode() == 200 else None

def request(handle, data = None):
    for i in xrange(3):
        try:
            r =  request_(handle, data)
            if r is None:
                print "OLOLO"
                continue
            return r
        except Exception as e:
            print e


def filter(problems, pos=tuple(), neg=tuple()):
    matched = [0] * 60
    other = [0] * 60

    for p in problems:
        if "solved" in p:
            continue
        for x in pos:
            if x not in p["operators"]:
                other[p["size"]] += 1
                break
        else:
            for x in neg:
                if x in p["operators"]:
                    other[p["size"]] += 1
                    break
            else:
                matched[p["size"]] += 1
    for a in ((x,y) for (x,y) in enumerate(zip(matched, other)) if y != (0,0)):
        print a


def gen_problems(mode, program_length, programs_count, ops):
    if mode == "train":
        for _ in xrange(programs_count):
            response = request("train",
                {"size": 42})
            print "Traing program: {0}, length: {1}, id: {2}".format(
                response["challenge"], response["size"], response["id"])
            yield response["id"]
        return
    elif mode == "solve":
        problems = request("myproblems")
        for sz in xrange(5, program_length + 1):
            for p in problems:
                if p["size"] == sz and "solved" not in p:
                    print "Found unsolved problem id: {0} ops:{1} last {2} len {3}".format(
                        p["id"], p["operators"], programs_count, p["size"])
                    yield p["id"]
                    programs_count -= 1
                    if not programs_count:
                        return
    print "Not enough problems!", programs_count


def check_args(problem, arguments):
    values = []
    for start in xrange(0, len(arguments), 256):
        response = request(
            "eval", {"id": problem, "arguments": arguments[start:start+256]})
        if response["status"] == "ok":
            print "Evaluated {0} values".format(len(response["outputs"]))
            values += response["outputs"]
        else:
            exit("/eval error: " + response["message"])
    return values


def guess(problem, program):
    print "guessing", program
    response = request("guess", {"id": problem, "program": program})
    if response["status"] == "win":
        print "Guessed!"
        return None
    elif response["status"] == "error":
        exit("/guess error: " + response["message"])
    else:
        print "Guess failed on: " + " ".join(response["values"])
        return response["values"]


def main():
    parser = ArgumentParser()
    parser.add_argument("solver", type=str, help="solver binary")
    parser.add_argument("mode", type=str, help="train/solve")
    parser.add_argument("program_length", type=int)
    parser.add_argument("--programs_count", type=int, default=1)
    parser.add_argument("--solver2", type=str, help="solver binary")
    parser.add_argument(
            "ops",
            type=str,
            nargs="*",
            help="ops in {if0, fold, tfold}")
    args = parser.parse_args()
    ops = set(("not", "shr1", "shr4", "shr16", "shl1",
        "or", "and", "xor", "plus"))
    if "if0" in args.ops:
        ops.add("if0")
    if "fold" in args.ops:
        ops.add("fold")
    if "tfold" in args.ops:
        ops.add("tfold")

    problems = gen_problems(
        args.mode,
        args.program_length,
        args.programs_count,
        ops)

    solver = subprocess.Popen(
        args.solver.split(),
    #    stderr=subprocess.STDERR,
        stdout=subprocess.PIPE,
        stdin=subprocess.PIPE)
    if args.solver2:
        solver2 = subprocess.Popen(
            args.solver2.split(),
        #    stderr=subprocess.STDERR,
            stdout=subprocess.PIPE,
            stdin=subprocess.PIPE)

    def send_to_solver(s):
        solver.stdin.write(s + "\n")
        solver.stdin.flush()
    def get_from_solver():
        s = solver.stdout.readline().strip()
        return s
    if args.solver2:
        def send_to_solver2(s):
            solver2.stdin.write(s + "\n")
            solver2.stdin.flush()
        def get_from_solver2():
            s = solver2.stdout.readline().strip()
            return s

    try:
        if get_from_solver() != "ready":
            exit("Solver is not ready")
        if (args.solver2):
            if get_from_solver2() != "ready":
                exit("Solver is not ready")
        print "READYYYYYYYYYYYYYYYYYYYY"
        time.sleep(2)
        for problem in problems:
            arguments = ["{0:#x}".format(int(s))
                for s in get_from_solver().split()]
            if args.solver2:
                arguments2 = ["{0:#x}".format(int(s))
                    for s in get_from_solver2().split()]
                if arguments != arguments2:
                    print "args mismatched!!!!!!!!!!!!!!!!!"
            values = check_args(problem, arguments)
            send_to_solver(" ".join(str(int(x, 16)) for x in values))
            if args.solver2:
                send_to_solver2(" ".join(str(int(x, 16)) for x in values))
            fail1 = False
            fail2 = False
            while not fail1:
                program = get_from_solver()
                fail1 = program == "fail"
                if fail1:
                    print "SOLVER 1  failed"
                    break

                wrong_values = guess(problem, program)
                if not wrong_values:
                    send_to_solver("win")
                    break
                send_to_solver("mismatch " + " ".join(
                    str(int(x, 16)) for x in wrong_values))

            if args.solver2:
                if not fail1:
                    program2 = get_from_solver2()
                    if program2 != "fail":
                        send_to_solver2("win")
                    continue

                while not fail2:
                    program2 = get_from_solver2()
                    fail2 = program2 == "fail"
                    if fail2:
                        print "SOLVER 2  failed"
                        break

                    wrong_values = guess(problem, program2)
                    if not wrong_values:
                        send_to_solver2("win")
                        break
                    send_to_solver2("mismatch " + " ".join(
                        str(int(x, 16)) for x in wrong_values))
    finally:
        solver.terminate()


if __name__ == "__main__":
    main()
