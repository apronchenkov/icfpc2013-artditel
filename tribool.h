#pragma once

typedef uint64_t Bits;

enum class Op {
    If0,
    Not,
    Shl1,
    Shr1,
    Shr4,
    Shr16,
    And,
    Or,
    Xor,
    Plus,

    Zero,
    One,
    Var
};

struct Tribool{
    Tribool():
        set(0), values(0) {}
    explicit Tribool(Bits c):
        set(~Bits(0)), values(c) {}
    Tribool(Bits set, Bits values):
        set(set), values(values & set) {}
    const Bits set;
    const Bits values;

    Bits ones() const {
        return set & values;
    }
    Bits zeros() const {
        return set & ~values;
    }
    Tribool operator ~() const {
        return {set, ~values};
    }
    Tribool operator >>(int cnt) const {
        return {
            (set >> cnt) | (((Bits(1) << cnt) - 1) << (64-cnt)),
            (values >> cnt)};
    }
    Tribool operator <<(int cnt) const {
        return {
            (set << cnt) | ((Bits(1) << cnt) - 1),
            (values << cnt)};
    }

    bool isConst() const {
        return !~set;
    }

    Bits value() const {
        return values;
    }

    Tribool determine(int value) const {
        if (value) {
            Tribool(values | ~set);
        } else {
            Tribool(values);
        }
    }
};

inline Tribool operator &(const Tribool& a, const Tribool& b) {
    return {
        (a.set & b.set) | (a.zeros() | b.zeros()),
        a.values & b.values
    };
}

inline Tribool operator *(const Tribool& a, const Tribool& b) {
    return {
        (a.set & b.set) & ~(a.values ^ b.values),
        a.values
    };
}

inline Tribool operator | (const Tribool& a, const Tribool& b) {
    return {
        (a.set & b.set) | (a.ones() | b.ones()),
        a.values | b.values
    };
}

inline Tribool operator ^(const Tribool& a, const Tribool& b) {
    return {
        (a.set & b.set),
        a.values ^ b.values
    };
}

inline Tribool operator +(const Tribool& a, const Tribool& b) {
    return { 0, 0 };
}

inline bool operator ==(const Tribool& a, const Tribool& b) {
    return a.set == b.set && a.values == b.values;
}

inline bool operator !=(const Tribool& a, const Tribool& b) {
    return ! (a == b);
}


inline Tribool eval(const Tribool& a, const Tribool& b, Op op) {

    switch (op) {
        case Op::And:
            return a & b;

        case Op::Or:
            return a | b;

        case Op::Xor:
            return a ^ b;

        case Op::Plus:
            return a + b;
    }
}
