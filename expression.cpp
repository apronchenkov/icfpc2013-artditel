#include "expression.h"


Bits eval(CExpression e, Bits input) {
    return (*e)(input);
}

std::string toString(CExpression e) {
    return std::string("(lambda (x) ") + e->str() + ")";
}

